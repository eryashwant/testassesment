

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setDashboardRoot()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func setDashboardRoot() {
      let rootVC = StoryboardScene.Main.instantiateViewController(withClass: TabBarController.self)
      let navigationController = UINavigationController(rootViewController: rootVC)
      navigationController.hidesBottomBarWhenPushed = false
      navigationController.navigationBar.isHidden = true
      navigationController.interactivePopGestureRecognizer?.isEnabled = false
      setRoot(viewController: navigationController)
    }

    private func setRoot(viewController: UIViewController) {
      if let windowObject = appDelegate.window {
        windowObject.rootViewController = nil
        windowObject.rootViewController = viewController
        windowObject.makeKeyAndVisible()
      }
    }
}
