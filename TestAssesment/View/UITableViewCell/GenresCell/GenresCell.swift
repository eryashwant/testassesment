

import UIKit

class GenresCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var genresLabel: UILabel!
    
    static let identifier = "GenresCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ items: Genres) {
        genresLabel.text = items.name ?? ""
    }
}
