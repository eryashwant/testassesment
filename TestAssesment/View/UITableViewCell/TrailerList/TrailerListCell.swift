

import UIKit

class TrailerListCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var trailerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ items: Trailers) {
        trailerNameLabel.text = items.name ?? ""
    }
}
