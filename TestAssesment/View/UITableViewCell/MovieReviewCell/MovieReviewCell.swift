

import UIKit

class MovieReviewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    
    // MARK: - Instance properties
    var callBackMoreDetail: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ items: MovieReview) {
        authorLabel.text = items.author ?? ""
        reviewLabel.text = items.content ?? ""
    }
    
    // MARK: - IBAction
    @IBAction func moreDetail(_ sender: Any) {
        callBackMoreDetail?()
    }
}
