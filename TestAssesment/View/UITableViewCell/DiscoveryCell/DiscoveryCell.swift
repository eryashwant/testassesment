

import UIKit

class DiscoveryCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    // MARK: - Instance properties
    static let identifier = "GenresCell"
    var callBackMovieReview: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ items: Discovery) {
        let imgUrl = "https://image.tmdb.org/t/p/w500" + (items.imageUrl ?? "")
        movieImageView.setImageWithPlaceholder(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "ic_defualtmovie"))
        
        titleLabel.text = items.title ?? ""
        overviewLabel.text = items.overview ?? ""
    }
    
    // MARK: - IBAction
    @IBAction func moviewReview(_ sender: Any) {
        callBackMovieReview?()
    }
    
}
