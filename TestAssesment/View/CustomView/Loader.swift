

import Foundation
import MBProgressHUD

class Loader {
    
    private static var loader: MBProgressHUD?
    static func show() {
        DispatchQueue.main.async {
            Loader.loader?.hide(animated: true)
            loader = MBProgressHUD.showAdded(to: Application.keyWindow, animated: true)
            loader?.detailsLabel.text = "Loading..."
            //Application.keyWindow.endEditing(true)
        }
    }
    
    static func hide() {
        DispatchQueue.main.async {
            Loader.loader?.hide(animated: true)
        }
    }
}
