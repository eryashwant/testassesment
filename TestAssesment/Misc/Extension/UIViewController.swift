

import Foundation
import UIKit

extension UIViewController {
    
    func pushVC(_ viewController: UIViewController, animated: Bool = true) {
        if Thread.isMainThread {
            self.navigationController?.pushViewController(viewController, animated: animated)
        } else {
            DispatchQueue.main.sync {
                self.navigationController?.pushViewController(viewController, animated: animated)
            }
        }
    }
}
