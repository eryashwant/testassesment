

import Foundation
import UIKit
extension UITableView {
    func reloadDataInMain() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    
    func showLoaderAtBottom( _ show: Bool) {
        DispatchQueue.main.async {
            if show {
                let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 70))
                self.tableFooterView = activityIndicator
                activityIndicator.color = .gray
                activityIndicator.startAnimating()
            } else {
                self.tableFooterView?.removeFromSuperview()
                self.tableFooterView = nil
            }
        }
    }
}
