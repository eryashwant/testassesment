
import Foundation
import UIKit

class StoryboardScene {
    
    static let Main: UIStoryboard = {
        return UIStoryboard(name: "Main", bundle: nil)
    }()
}

extension UIStoryboard {
    
    func controllerExists(withIdentifier: String) -> Bool {
        if let availableIdentifiers = self.value(forKey: "identifierToNibNameMap") as? [String: Any] {
            return availableIdentifiers[withIdentifier] != nil
        }
        
        return false
    }
    
    func instantiateViewController<T>(withClass: T.Type) -> T {
        // swiftlint:disable force_cast
        let identifier = NSStringFromClass(withClass as! AnyClass).components(separatedBy: ".")[1]
        guard self.controllerExists(withIdentifier: identifier) else {
            fatalError("Failed to instantiate viewController")
        }
        
        return self.instantiateViewController(withIdentifier: identifier) as! T
        // swiftlint:enable force_cast
    }
    
}
