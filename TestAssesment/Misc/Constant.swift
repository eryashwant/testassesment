

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate // swiftlint:disable:this force_cast

enum Application {
    
    //Applicatipn keyWindow
    static var keyWindow: UIWindow {
        return UIApplication.shared.keyWindow!
    }
}

// MARK: - Screen size
struct Screen {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let bounds = UIScreen.main.bounds
    static let maxLength = max(width, height)
    static let minLength = min(width, height)
}
