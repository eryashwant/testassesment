

import Foundation
import Kingfisher

struct KingfisherHelper {
  
  static func configure(maxCacheSize: UInt, maxCachePeriod: TimeInterval) {
    ImageCache.default.diskStorage.config.sizeLimit = maxCacheSize
    ImageCache.default.diskStorage.config.expiration = .seconds(maxCachePeriod)
    
    KingfisherManager.shared.cache.diskStorage.config.pathExtension = "jpg"
  }
}

extension UIImageView {
  
  func setImage(with url: URL) {
    self.kf.indicatorType = .activity
    self.kf.setImage(with: url)
  }
  
  func setImage(with url: URL, size: CGSize) {
    
    let processor = ResizingImageProcessor(referenceSize: size, mode: .aspectFill)
    
    self.kf.indicatorType = .activity
    self.kf.setImage(with: url, options: [.processor(processor)])
  }
  
  func setImageWithPlaceholder(with url: URL?, placeholderImage: UIImage, indicatorType: IndicatorType = .none) {
    self.kf.indicatorType = indicatorType
    self.kf.setImage(with: url, placeholder: placeholderImage)
  }
  
  func cancelDownload() {
    self.kf.cancelDownloadTask()
  }
}

extension UIButton {
  
  func setImage(with url: URL) {
    self.kf.setImage(with: url, for: .normal)
  }
  
  func cancelDownload() {
    self.kf.cancelImageDownloadTask()
  }
}
