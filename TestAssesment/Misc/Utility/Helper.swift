
import Foundation
import UIKit
import ObjectMapper

struct Alert {
    
    static func showAlertWithMessage(_ message: String,
                                     title: String = "TestAssesment",
                                     handler:(() -> ())? = nil) {
        
        //** If any Alert view is alrady presented then do not show another alert
        var viewController: UIViewController!
        
        if let vc  = UIApplication.topViewController() {
            if (vc.isKind(of: UIAlertController.self)) {
                return
            } else {
                viewController = vc
            }
        } else {
            viewController = appDelegate.window?.rootViewController!
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (_) in
            handler?()
        })
        
        viewController!.present(alert, animated: true)
    }
    
}

extension UIApplication {
    
    static public func currentViewController() -> UIViewController? {
        var currentViewController = UIApplication.topViewController()
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        return currentViewController
    }
    
    static func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(selected)
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension Data {
   func deserialize() -> [String: Any]? {
       do {
           let object = try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any]
           print("\(String(describing: object) )")
           return object
       }
       catch let error {
           print(error.localizedDescription)
           return nil
       }
   }
}

let IntToStringTransform = TransformOf<String, Int>(fromJSON: { (value: Int?) -> String? in
    guard let val = value else { return nil }
    return "\(val)"
}, toJSON: { (value: String?) -> Int? in
    if let value = value { return Int(value) }
    return nil
})
