
import Foundation
import ObjectMapper

struct Genres: Mappable, Hashable {
    
    var id: String?
    var name: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- (map["id"], IntToStringTransform)
        name <-  map["name"]
    }
}
