

import Foundation
import ObjectMapper

struct MovieReview: Mappable, Hashable {
    
    var author: String?
    var content: String?
    var url: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        author <- map["author"]
        content <-  map["content"]
        url <- map["url"]
    }
}
