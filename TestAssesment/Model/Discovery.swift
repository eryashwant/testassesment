
import Foundation
import ObjectMapper

struct Discovery: Mappable, Hashable {
    
    var id: String?
    var title: String?
    var overview: String?
    var imageUrl: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- (map["id"], IntToStringTransform)
        title <-  map["title"]
        overview <- map["overview"]
        imageUrl <- map["poster_path"]
    }
}
