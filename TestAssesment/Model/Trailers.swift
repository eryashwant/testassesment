
import Foundation
import ObjectMapper

struct Trailers: Mappable, Hashable {
    
    var name: String?
    var trailerKey: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        trailerKey <- map["key"]
    }
    
}
