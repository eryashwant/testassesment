

import UIKit

class MovieReviewVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var reviewTable: UITableView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    // MARK: - Instance properties
    let viewModel = MovieReviewVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        getReview()
        viewModel.callReviewListApi()
        movieTitleLabel.text = viewModel.discovery?.title ?? ""
    }
    
    private func getReview() {
        viewModel.callBack = {
            self.reviewTable.reloadData()
            if self.viewModel.isInitialFetchCompleted {
                self.reviewTable.showLoaderAtBottom(false)
            }
        }
    }
}

// MARK: - Table view datasource methods

extension MovieReviewVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.movieReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieReviewCell", for: indexPath) as? MovieReviewCell
        let movieReview = viewModel.movieReview[indexPath.row]
        cell?.configure(movieReview)
        cell?.callBackMoreDetail = {
            let reviewDetailVC = StoryboardScene.Main.instantiateViewController(withClass: ReviewDetailVC.self)
            reviewDetailVC.movieReview = movieReview
            self.pushVC(reviewDetailVC)
        }
        return cell ?? UITableViewCell()
    }
}

// MARK: - Table view delegate methods

extension MovieReviewVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

// MARK: - Helper methods
extension MovieReviewVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffsetY = scrollView.contentOffset.y
        let totalContentHeight = scrollView.contentSize.height
        let screenHeight = Screen.height
        
        let maxOffsetY = totalContentHeight - (screenHeight + 100)
        
        if currentOffsetY > maxOffsetY && viewModel.isNextPageAvailable {
            if !viewModel.isFetching {
                viewModel.isFetching = true
                viewModel.isInitialFetchCompleted = true
                reviewTable.showLoaderAtBottom(true)
                viewModel.callReviewListApi()
            }
        }
    }
}
