

import Foundation
import Alamofire

class MovieReviewVM {
    
    // MARK: - Type properties
    var movieReview = [MovieReview]()
    var callBack: (() -> Void)?
    var discovery: Discovery?
    
    // MARK: - Pagination properties
    var currentPage = 1
    var isFetching = false
    var isNextPageAvailable = true
    var isInitialFetchCompleted = false
    
    func callReviewListApi() {
        getReviewList(page: currentPage)
    }
    
    // MARK: - DiscoveryList Api
    func getReviewList(page: Int) {
        isFetching = true
        if !isInitialFetchCompleted {
            Loader.show()
        }
       
        //let parameters = ["movie_id": Int(discovery?.id ?? "")]
       let movieId = Int(discovery?.id ?? "")
    AF.request("https://api.themoviedb.org/3/movie/\(movieId!)/reviews?api_key=95ef0726b22afef33ba5143669377970&language=en-US&page=\(page)").responseData { response in
            
            self.isFetching = false
            Loader.hide()
            
            if let object = response.data?.deserialize(), let discovery = object["results"] as? [[String: Any]] {
                
                let currentPage = object["page"] as? Int ?? 1
                let lastPage = object["total_pages"] as? Int ?? 1
                self.isNextPageAvailable = currentPage < lastPage ? true : false
                
                if discovery.isEmpty {
                   Alert.showAlertWithMessage("No review available")
                } else {
                    let movieReviewList = discovery.compactMap({ MovieReview(JSON: $0) })
                    self.movieReview.append(contentsOf: movieReviewList)
                    self.callBack?()
                    
                    if !self.isInitialFetchCompleted {
                        self.isInitialFetchCompleted = true
                    }
                    self.currentPage += 1
                }
            }
        }
    }
}
