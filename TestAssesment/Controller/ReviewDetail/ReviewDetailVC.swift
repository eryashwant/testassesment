

import UIKit
import WebKit

class ReviewDetailVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: - Instance properties
    var movieReview: MovieReview?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.navigationDelegate = self
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        loadContent()
    }

    private func loadContent() {
        let url = URL(string: movieReview?.url ?? "")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
}

// MARK: - WebViewDelegate

extension ReviewDetailVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Loader.show()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Loader.hide()
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
