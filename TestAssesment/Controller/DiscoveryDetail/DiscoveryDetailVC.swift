

import UIKit

class DiscoveryDetailVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    // MARK: - Type properties
    var discovery: Discovery?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        configureData()
        //setupView()
    }
    
    private func setupView() {
        DispatchQueue.main.async {
            self.movieImageView.contentMode = .scaleAspectFill
        }
        
    }
    
    private func configureData() {
        if let items = discovery {
            let imgUrl = "https://image.tmdb.org/t/p/w500" + (items.imageUrl ?? "")
            movieImageView.setImageWithPlaceholder(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "ic_defualtmovie"))
            titleLabel.text = items.title ?? ""
            overviewLabel.text = items.overview ?? ""
        }
    }
    
    // MARK: - IBAction
    @IBAction func officialTrailer(_ sender: Any) {
        let trailerListVC = StoryboardScene.Main.instantiateViewController(withClass: TrailerListVC.self)
        trailerListVC.viewModel.discovery = discovery
        self.pushVC(trailerListVC)
    }
}
