

import UIKit

class DiscoveryVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var discoveryTableView: UITableView!
    
    // MARK: - Instance properties
    private let viewModel = DiscoveryVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        getDiscovery()
        viewModel.callDiscoveryListApi()
    }
    
    private func getDiscovery() {
        viewModel.callBack = {
            self.discoveryTableView.reloadData()
            if self.viewModel.isInitialFetchCompleted {
                self.discoveryTableView.showLoaderAtBottom(false)
            }
        }
    }
}

// MARK: - Table view datasource methods

extension DiscoveryVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.discovery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoveryCell", for: indexPath) as? DiscoveryCell
        let discovery = viewModel.discovery[indexPath.row]
        cell?.configure(discovery)
        cell?.callBackMovieReview = {
            let movieReviewVC = StoryboardScene.Main.instantiateViewController(withClass: MovieReviewVC.self)
            movieReviewVC.viewModel.discovery = self.viewModel.discovery[indexPath.row]
            self.pushVC(movieReviewVC)
        }
        return cell ?? UITableViewCell()
    }
}

// MARK: - Table view delegate methods

extension DiscoveryVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let discoveryDetailVC = StoryboardScene.Main.instantiateViewController(withClass: DiscoveryDetailVC.self)
        discoveryDetailVC.discovery = viewModel.discovery[indexPath.row]
        self.pushVC(discoveryDetailVC)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

// MARK: - Helper methods
extension DiscoveryVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffsetY = scrollView.contentOffset.y
        let totalContentHeight = scrollView.contentSize.height
        let screenHeight = Screen.height
        
        let maxOffsetY = totalContentHeight - (screenHeight + 100)
        
        if currentOffsetY > maxOffsetY && viewModel.isNextPageAvailable {
            if !viewModel.isFetching {
                viewModel.isFetching = true
                viewModel.isInitialFetchCompleted = true
                discoveryTableView.showLoaderAtBottom(true)
                viewModel.callDiscoveryListApi()
            }
        }
    }
}
