
import Foundation
import Alamofire

class DiscoveryVM {
    
    // MARK: - Type properties
    var discovery = [Discovery]()
    var callBack: (() -> Void)?
    
    // MARK: - Pagination properties
    var currentPage = 1
    var isFetching = false
    var isNextPageAvailable = true
    var isInitialFetchCompleted = false
    
    func callDiscoveryListApi() {
        getDiscoveryList(page: currentPage)
    }
    
    // MARK: - DiscoveryList Api
    func getDiscoveryList(page: Int) {
        isFetching = true
        if !isInitialFetchCompleted {
            Loader.show()
        }
    AF.request("https://api.themoviedb.org/3/discover/movie?api_key=95ef0726b22afef33ba5143669377970&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=\(page)").responseData { response in
            
            self.isFetching = false
            Loader.hide()
            
            if let object = response.data?.deserialize(), let discovery = object["results"] as? [[String: Any]] {
                
                let currentPage = object["page"] as? Int ?? 1
                let lastPage = object["total_pages"] as? Int ?? 1
                self.isNextPageAvailable = currentPage < lastPage ? true : false
                
                let discoveryList = discovery.compactMap({ Discovery(JSON: $0) })
                self.discovery.append(contentsOf: discoveryList)
                self.callBack?()
                
                if !self.isInitialFetchCompleted {
                    self.isInitialFetchCompleted = true
                }
                self.currentPage += 1
            }
        }
    }
}
