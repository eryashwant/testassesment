

import Foundation
import Alamofire

class GenresVM {
    
    // MARK: - Type properties
    var genres = [Genres]()
    
    func getGenersList(_ complition: @escaping (() -> Void)) {
        Loader.show()
        AF.request("https://api.themoviedb.org/3/genre/movie/list?api_key=95ef0726b22afef33ba5143669377970&language=en-US").responseData { response in
            Loader.hide()
            if let object = response.data?.deserialize(), let geners = object["genres"] as? [[String: Any]] {
                let genersList = geners.compactMap({ Genres(JSON: $0) })
                self.genres.append(contentsOf: genersList)
                complition()
            }
        }
    }
    
}
