
import UIKit

class GenresVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var genersTableView: UITableView!
    
    // MARK: - Instance properties
    private let viewModel = GenresVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        getGeners()
    }
    
    private func getGeners() {
        viewModel.getGenersList {
            self.genersTableView.reloadData()
        }
    }
}

// MARK: - Table view datasource methods

extension GenresVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenresCell", for: indexPath) as? GenresCell
        let geners = viewModel.genres[indexPath.row]
        cell?.configure(geners)
        return cell ?? UITableViewCell()
    }
    
}

// MARK: - Table view delegate methods

extension GenresVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
