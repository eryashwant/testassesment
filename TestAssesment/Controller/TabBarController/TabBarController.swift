

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    
    private func initialSetup() {
        addInitialControllers()
    }
    
    private func addInitialControllers() {
        let genresVC = self.storyboard?.instantiateViewController(identifier: "GenresVC") as? GenresVC
        let genresNav = UINavigationController.init(rootViewController: genresVC!)
        
        let discoveryVC = self.storyboard?.instantiateViewController(identifier: "DiscoveryVC") as? DiscoveryVC
        let discoveryNav = UINavigationController.init(rootViewController: discoveryVC!)
        
        //Initialize controllers
        viewControllers = [genresNav, discoveryNav]
           
        genresNav.tabBarItem = UITabBarItem(title: "Genres",
                                          image: UIImage(named: ""),
                                          selectedImage: UIImage(named: ""))
        
        discoveryNav.tabBarItem = UITabBarItem(title: "Discovery",
                                          image: UIImage(named: ""),
                                          selectedImage: UIImage(named: ""))
    }
}
