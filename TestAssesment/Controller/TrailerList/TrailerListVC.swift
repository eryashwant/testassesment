

import UIKit

class TrailerListVC: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var trailerListTableView: UITableView!
    
    // MARK: - Instance properties
    let viewModel = TrailerListVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        configureTableView()
        getTrailerList()
        viewModel.callTrailerListApi()
    }

    private func configureTableView() {
        self.trailerListTableView.estimatedRowHeight = 90
        self.trailerListTableView.rowHeight = UITableView.automaticDimension
    }
    
    private func getTrailerList() {
        viewModel.callBack = {
            self.trailerListTableView.reloadData()
        }
    }
}

// MARK: - Table view datasource methods

extension TrailerListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.trailers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrailerListCell", for: indexPath) as? TrailerListCell
        let trailer = viewModel.trailers[indexPath.row]
        cell?.configure(trailer)
        return cell ?? UITableViewCell()
    }
}

// MARK: - Table view delegate methods

extension TrailerListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trailer = viewModel.trailers[indexPath.row]
        let trailerVC = StoryboardScene.Main.instantiateViewController(withClass: TrailerVC.self)
        trailerVC.trailers = trailer
        self.pushVC(trailerVC)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}
