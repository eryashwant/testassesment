

import Foundation
import Alamofire

class TrailerListVM {
    
    // MARK: - Type properties
    var trailers = [Trailers]()
    var callBack: (() -> Void)?
    var discovery: Discovery?
    
    func callTrailerListApi() {
        getTrailerList()
    }
    
    // MARK: - DiscoveryList Api
    func getTrailerList() {
        
        Loader.show()
        
        let movieId = Int(discovery?.id ?? "")
        AF.request("https://api.themoviedb.org/3/movie/\(movieId!)/videos?api_key=95ef0726b22afef33ba5143669377970&language=en-US").responseData { response in

            Loader.hide()
            
            if let object = response.data?.deserialize(), let videoList = object["results"] as? [[String: Any]] {
                
                if videoList.isEmpty {
                    Alert.showAlertWithMessage("No review available")
                } else {
                    let trailersList = videoList.compactMap({ Trailers(JSON: $0) })
                    self.trailers.append(contentsOf: trailersList)
                    self.callBack?()
                }
            }
        }
    }
}
